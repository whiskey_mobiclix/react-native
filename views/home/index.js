import React from "react"
import { connect } from "react-redux"
import Layout from "../../Layout"
import { View, Image } from "react-native"
import CustomText from "../../components/custom-text"
import StandardButton from "../../components/buttons/standard"
import HexagonButton from "../../components/buttons/hexagon"
import SlotMachine from "./slot-machine"

//Configs
import { nav } from "./config"

class Home extends React.Component{
	render(){
		return (
			<Layout name="home">
				<View style={{
					paddingTop: 50,
					padding: 15,
				}}>
					<View style={{
						flexDirection: "row",
					}}>
						{
							nav.map((o, ix) => 
								<StandardButton
									key={ix}
									active={ix === 0}
									textStyle={{
										fontSize: 15,
										fontStyle: "italic",
									}}
									style={{
										fontSize: 15,
										fontStyle: "italic",
										flex: 1,
									}}>
									{o.name}
								</StandardButton>
							)
						}
					</View>

					<View style={{
						marginTop: 50,
						flexDirection: "row",
						alignItems: "center",
						justifyContent: "center",
					}}>
						<SlotMachine />
					</View>
				</View>
			</Layout>
		)
	}
}

export default Home