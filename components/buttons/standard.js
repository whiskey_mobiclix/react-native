import React from "react"
import { View } from "react-native"
import CustomText from "../../components/custom-text"
import { Svg, Polygon } from "react-native-svg"

class StandardButton extends React.Component{
	state = {
		width: 0,
		height: 0,
		ready: false,
	}

	render(){
		return (
			<View
				style={{
					...this.props.style || {},
					alignSelf: "flex-start",
					position: "relative",
					alignItems: "center",
					marginBottom: 0,
					padding: 7,
					paddingLeft: 10,
					paddingRight: 10,
					opacity: !this.state.ready ? 0 : 1,
					margin: -2,
				}}
				onLayout={e => {
					const {width, height} = e.nativeEvent.layout

					setTimeout(() => {
						this.setState({
							width,
							height,
							ready: true,
						})
					}, 100)
				}}
			>
				<CustomText style={Object.assign({
					...this.props.textStyle || {},
		        	zIndex: 2,
		        }, (this.props.active ? {
		        	textShadowColor: "rgba(0,0,0,.3)",
					textShadowRadius: 1,
					textShadowOffset: {
						width: 1,
						height: 1,
					}
		        } : {}))}>
		        	{this.props.children}
		        </CustomText>

			    <View style={{
			    	position: "absolute",
					zIndex: 1,
					alignSelf: "flex-start",
					top: 0,
					left: 0,
					right: 0,
					bottom: 0,
			    }}>
					<Svg height="100%" width="100%">
				        <Polygon
				          points={`
				          	${this.state.height * .3} 1
				          	1 ${this.state.height - 1}
				          	${this.state.width - this.state.height * .3 - 1} ${this.state.height - 1}
				          	${this.state.width - 1} 1
				          `}
				          fill={`${this.props.active ? "rgb(251,112,4)" : "transparent"}`}
				          stroke="#fff"
				          strokeWidth="1"
				        />
				    </Svg>
			    </View>
			</View>
		)
	}
}

export default StandardButton

