export const setActiveScene = value => dispatch => {
	return dispatch({
		type: "SET_ACTIVE_SCENE",
		payload: value,
	})
}