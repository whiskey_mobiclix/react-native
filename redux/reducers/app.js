import { combineReducers } from "redux"

const app = (state = {
	scene: "",
}, action) => {
  switch (action.type) {
  	case "SET_ACTIVE_SCENE":
  		return {
  			...state,
  			scene: action.payload,
  		}
    default:
    	return state
  }
}

export default app