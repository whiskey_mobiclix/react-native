import React, { Component } from 'react';
import { Provider } from "react-redux"
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './RootNavigation';
import { SafeAreaView, AppRegistry, StatusBar, View } from 'react-native';
import store from "./redux/store"
import Routes from './Routes.js'
import * as ExpoFont from "expo-font"

class App extends Component {
	state = {
		ready: false,
	}

	async componentDidMount(){
		await ExpoFont.loadAsync({
			"FF Mark Pro": require("./assets/fonts/FF-Mark-Pro/FF-Mark-Pro-Book.otf"),
			"FF Mark Pro Medium": require("./assets/fonts/FF-Mark-Pro/FF-Mark-Pro-Medium.otf"),
			"FF Mark Pro Bold": require("./assets/fonts/FF-Mark-Pro/FF-Mark-Pro-Bold.otf"),
			"FF Mark Pro Light": require("./assets/fonts/FF-Mark-Pro/FF-Mark-Pro-Light.otf"),
			"FF Mark Pro Italic": require("./assets/fonts/FF-Mark-Pro/FF-Mark-Pro-Book-Italic.otf"),
			"Abilition": require("./assets/fonts/Abilition/Abolition-Round.ttf"),
			"Abilition Italic": require("./assets/fonts/Abilition/Abolition-Oblique.ttf"),
		})

		this.setState({
			ready: true,
		})
	}

   	render() {
      return (
      	<NavigationContainer ref={navigationRef}>
	      	<Provider store={store}>
		        <View style={{
		          	flex: 1,
		          	backgroundColor: "#1c1c1c",
		        }}>
		          <StatusBar hidden={false} barStyle="light-content"/>
		          <SafeAreaView style={{
		          	flex: 1,
		          }}>
		          	<View style={{
		          		flex: 1,
		          	}}>
		          		{
		          			this.state.ready
		          			?
		          			<Routes />
		          			:
		          			null
		          		}
		          	</View>
		          </SafeAreaView>
		        </View>
	        </Provider>
        </NavigationContainer>
      )
   	}
}
export default App
AppRegistry.registerComponent('app', () => App)