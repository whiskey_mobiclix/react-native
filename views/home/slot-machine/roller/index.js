import React from "react"
import { View, Animated } from "react-native"
import { LinearGradient } from 'expo-linear-gradient'
import Item from "./item"

const shuffle = arr => {
	  let array = arr
	  let currentIndex = array.length, temporaryValue, randomIndex;

	  while (0 !== currentIndex) {
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }

	  return array;
}

const Roller = props => {
	let top = React.useRef(new Animated.Value(0)).current
	let [rewards, setRewards] = React.useState(shuffle([
		require("../../../../assets/icons/coin.png"),
		require("../../../../assets/icons/diamond.png"),
		require("../../../../assets/icons/lightning.png")
	]))

	React.useEffect(() => {
		if(props.role){
			setTimeout(() => {
				top.setValue(0)

				Animated.timing(
			      top,
			      {
			        toValue: -200 * 10,
			        duration: 1000,
			      }
			    ).start()
			}, props.delay)
		}
	}, [props.role])

	return (
		<LinearGradient
			colors={['rgb(175,195,222)', 'rgb(255,255,255)', 'rgb(175,195,222)']}
			style={{
				flex: 1,
				borderWidth: 0,
				borderColor: "#fff",
				borderRadius: 8,
				overflow: "hidden",
				marginLeft: 5,
				marginRight: 5,
			}}
		>
			<Animated.View style={{
				flex: 1,
				position: "relative",
				top: top,
			}}>
				{
					["","","","","","","","","","","",""].map((o, ix) =>
						<View style={{
							key: ix,
							flex: 1,
							position: "absolute",
							width: "100%",
							height: "100%",
							top: `${ix * 100}%`,
						}}>
							{
								rewards.map((o, ix) =>
									<Item icon={o} key={ix}/>
								)
							}
						</View>
					)
				}
			</Animated.View>
		</LinearGradient>
	)
}

export default Roller