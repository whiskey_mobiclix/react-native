import React from "react"
import { Text, PixelRatio, Dimensions } from "react-native"

const { width, height } = Dimensions.get("window"), scale = width / 320

class CustomText extends React.Component{
	font = () => {
		if(!this.props.style)
			return "Abilition"

		if(this.props.style.fontStyle === "italic")
			return "Abilition Italic"

		return "Abilition"
	}

	size = () => {
		if(!this.props.style || !this.props.style.fontSize) return 16
		return Math.round(this.props.style.fontSize * scale)
	}

	render(){
		return (
			<Text {...{
				...this.props,
				numberOfLines: 1,
				style: {
					...this.props.style,
					color: this.props.style && this.props.style.color || "#fff",
					fontSize: this.size(),
					fontFamily: this.font(),
				}
			}}>{this.props.children}</Text>	
		)
	}
}

export default CustomText