import React from "react"
import { View, Image } from "react-native"
import CustomText from "../../../../../components/custom-text"

const RollerItem = props => {
	return (
		<View
			style={{
				borderWidth: 0,
				borderColor: "#fff",
				flex: 1,
				justifyContent: "center",
				alignItems: "center",
			}}
			onLayout={e => {

			}}
		>
			<Image
				style={{
					width: 50,
					height: 50,
					resizeMode: "cover",
				}}
				source={props.icon}
			/>
		</View>
	)
}

export default RollerItem