import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Text } from "react-native"
import Home from './views/home'

const Stack = createStackNavigator();

const Routes = () => (
	<Stack.Navigator screenOptions={{
	    headerShown: false
	}}>
	    <Stack.Screen name="Home" component={Home} />
	</Stack.Navigator>
)

export default Routes