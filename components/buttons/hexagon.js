import React from "react"
import { View, Image, TouchableWithoutFeedback } from "react-native"
import CustomText from "../../components/custom-text"
import { Svg, Polygon, Defs, LinearGradient, Stop } from "react-native-svg"

class HexagonButton extends React.Component{
	state = {
		width: 0,
		height: 0,
		ready: false,
	}

	render(){
		return (
			<TouchableWithoutFeedback onPress={() => {
				this.props.onPress && this.props.onPress()
			}}>
				<View
					style={{
						...this.props.style || {},
						alignSelf: "flex-start",
						position: "relative",
						flexDirection: this.props.style && this.props.style.flexDirection || "row",
						marginBottom: 0,
						padding: 2,
						paddingLeft: this.props.style && this.props.style.paddingLeft || 10,
						paddingRight: this.props.style && this.props.style.paddingRight || 10,
						height: this.state.ready && this.props.balance ? this.state.width * .8 : "auto",
						opacity: !this.state.ready ? 0 : 1,
					}}
					onLayout={e => {
						const {width, height} = e.nativeEvent.layout

						this.setState({
							width,
							height: this.props.balance ? width * .8 : height,
						})

						setTimeout(() => {
							this.setState({
								ready: true,
							})
						}, 10)
					}}
				>
					{
						this.props.icon
						&&
						<Image style={{
							height: this.state.height - 8,
							width: this.state.height - 8,
							zIndex: 2,
							top: 3,
							marginRight: 5,
						}} source={this.props.icon}/>
					}

					<CustomText style={{
						...this.props.textStyle || {},
			        	zIndex: 3,
			        }}>
			        	{this.props.children}
			        </CustomText>

				    <View style={{
				    	position: "absolute",
						zIndex: 1,
						top: 0,
						left: 0,
						right: 0,
						bottom: 0,
				    }}>
				    	{
				    		this.state.ready
				    		&&
				    		<Svg height="100%" width="100%">
					    		<Defs>
								    <LinearGradient id="grad" x1="0%" y1="100%" x2="0%" y2="0%">
									  <Stop offset="0%" stopColor="rgb(147,23,255)" stopOpacity="1" />
									  <Stop offset="50%" stopColor="rgb(19,198,255)" stopOpacity="1" />
									  <Stop offset="100%" stopColor="rgb(3,80,180)" stopOpacity="1" />
									</LinearGradient>
								</Defs>

						        <Polygon
						          points={`
						          	${this.state.height * .25} 1
						          	1 ${this.state.height / 2}
						          	${this.state.height * .25} ${this.state.height - 1}
						          	${this.state.width - this.state.height * .25} ${this.state.height - 1}
						          	${this.state.width - 1} ${this.state.height / 2}
						          	${this.state.width - this.state.height * .25} 1
						          `}
						          fill={`${this.props.active ? "url(#grad)" : "rgb(170,170,170)"}`}
						          stroke="#fff"
						          strokeWidth="2"
						        />
						    </Svg>
				    	}
				    </View>
				</View>
			</TouchableWithoutFeedback>
		)
	}
}

export default HexagonButton

