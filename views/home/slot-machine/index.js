import React from "react"
import { View, ImageBackground } from "react-native"
import Roller from "./roller"
import HexagonButton from "../../../components/buttons/hexagon"
import CustomText from "../../../components/custom-text"
import { Svg, Polygon } from "react-native-svg"

const SlotMachine = props => {
	const [spinning, setSpinning] = React.useState(false)

	const spin = () => {
		setSpinning(true)

		setTimeout(() => {
			setSpinning(false)
		}, 2000)
	}

	return (
		<View>
			<View style={{
				flexDirection: "row",
				justifyContent: "center",
				marginBottom: 20,
			}}>
				<CustomText style={{
					fontSize: 25,
					fontStyle: "italic",
				}}>
					Slot machine
				</CustomText>
			</View>

			<ImageBackground
				source={require("../../../assets/backgrounds/slot-machine.png")}
				style={{
					width: 300,
					height: 250,
					flexDirection: "row",
					justifyContent: "center",
					alignItems: "center",
					borderColor: "#fff",
					borderWidth: 0,
					position: "relative",
				}}
			>
				<View
					style={{
						width: 220,
						height: 200,
						borderColor: "#fff",
						borderWidth: 0,
						flexDirection: "row",
						overflow: "hidden",
					}}
				>
					<Roller delay={0} role={spinning} />
					<Roller delay={200} role={spinning} />
					<Roller delay={400} role={spinning} />
				</View>
			</ImageBackground>

			<View style={{
				marginTop: 20,
				flexDirection: "row",
				justifyContent: "center",
			}}>
				<HexagonButton
					active={true}
					style={{
						paddingLeft: 30,
						paddingRight: 30,
					}}
					textStyle={{
						fontSize: 25,
						fontStyle: "italic",
					}}
					onPress={() => {
						spin()
					}}
				>
					SPIN
				</HexagonButton>
			</View>
		</View>
	)
}

export default SlotMachine