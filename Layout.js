import React from "react"
import { connect } from "react-redux"
import { SafeAreaView, View } from "react-native"
import { setActiveScene } from "./redux/actions/app"
import { useIsFocused } from '@react-navigation/native' 

const Layout = props => {
	const isFocused = useIsFocused();
	if(isFocused) props.setActiveScene(props.name)

	return (
		<View style={{
			flex: 1,
			backgroundColor: "#1c1c1c",
		}}>
			{props.children}
		</View>
	)
}

export default connect(
	state => ({}),
	{
		setActiveScene,
	}
)(Layout)